\documentclass[aspectratio=169, xcolor=dvipsnames]{beamer}
\usetheme{guadec}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{listings}
\usepackage{ragged2e}
\usepackage{tikz}
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{xcolor}
\usepackage{lmodern}
\usepackage{animate}
\usepackage{pifont}% http://ctan.org/pkg/pifont

\usepackage[absolute,overlay]{textpos}
% \usepackage{FiraSans}

%-------------------------------
% Tick and cross
%-------------------------------
\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

%---------------------------------------------------------------------
% Fonts
%---------------------------------------------------------------------

% General

% Declare fontfamilys
\if@doNoFlama%
	% Sans serif math option
	\if@doSans%
	% Sans serif math
		\usepackage{fontspec}%
		\setmainfont{Arial Narrow}%
	\else%
		% Serif math
		\usefonttheme{professionalfonts}%
		\usepackage[no-math]{fontspec}%
	\fi%
	
	\newfontfamily\Light{Arial}%
	\newfontfamily\Book{Arial-Bold}%
	\newfontfamily\Medium{Arial-Bold}%
	\setsansfont{Arial}%
\else%
	% Sans serif math option
	\if@doSans%
	% Sans serif math
		\usepackage{fontspec}%
		\setmainfont{Roboto-Regular}%
	\else%
		% Serif math
		\usefonttheme{professionalfonts}%
		\usepackage[no-math]{fontspec}%
	\fi%
	
	\newfontfamily\Light{Roboto-Light}%
	\newfontfamily\Book{Roboto-Regular}%
	\newfontfamily\Medium{Roboto-Medium}%
	\newfontfamily\Condensed{Sintony-Bold}%
	\setsansfont{Roboto}%
\fi%

%% Titlepage
\setbeamerfont{title}{family=\Condensed,size=\fontsize{21}{24}}

% ----------------------------------------
%         END OF FONT SETUP
% ----------------------------------------



\title[CIM@LAB 2019]{Complex anatomical patterns in mild cognitive impairment to Alzheimer's disease conversion}
\author{\vskip0.2em\hskip-0.3cm Santiago Silva, Diana L. Giraldo, Eduardo Romero}
\date{\hskip-0.3cm Dic 2, 2019\vskip1em}

% Header no frame title
\newcommand{\header}[1]{%
    \par\vspace{.75cm}\noindent\textcolor{guadecblue}{\usebeamerfont{frametitle}#1}%
    \par\noindent\ignorespaces%
    
}
\newcommand\tikzmark[1]{
  \tikz[remember picture,overlay] \coordinate (#1);
}
%% DO not touch (Command to format the footnotes
\setbeamertemplate{footline}{
    \vspace{0.8cm}
}
% Disable "Figure" label for picture caption
\usepackage[labelformat=empty]{caption}
\captionsetup{
  format = plain,
  font = scriptsize,
  textfont = sc,
}

% Footnotes alphabetic
\renewcommand*{\thefootnote}{\alph{footnote}}
\renewcommand{\footnotesize}{\scriptsize}

% Define some colors
\definecolor{maleColor}{HTML}{3b5ef9}
\definecolor{femaleColor}{HTML}{f2219f}



% ========= DOCUMENT BEGINS ===================
\begin{document}

\begin{frame} 
\titlepage
\end{frame}

% \begin{frame}{Outline}
%   \setbeamertemplate{section in toc}[sections numbered]
%   \tableofcontents[hideallsubsections]
% \end{frame}

% {
% \setbeamertemplate{background}{}
% \usebackgroundtemplate{\includegraphics[width=\paperwidth]{img/path}}
% \setbeamercolor{section in toc}{fg=white}
% \setbeamertemplate{section in toc}[sections numbered]
% 
% \begin{frame}{\color{white} Outline}
% 	\vskip1cm
% 	\begin{columns}
% 		\column{0.27\textwidth}
% 		\begin{box_blue}{}
% 			\tableofcontents[hideallsubsections]
% 			\vspace{1em}
% 		\end{box_blue}
% 		\column{0.7\textwidth}
% 
% 	\end{columns}
% \end{frame}
% }


%% Introduction
% What AD is
\section{Introduction}
\framecard[light_blue]{{\color{white} \Huge{\textbf{INTRODUCTION}}}}
\begin{frame}{Mild Cognitive Impairment (MCI)}
\begin{columns}
	\column{0.5\textwidth}
	MCI is a \alert{cognitive decline} greater than expected for certain age and education level.\footnotemark
	
	\begin{box_orange}{\Large However...}
		\centering
		This decline must not be severe enough to notably interfere with  activities of daily living.
	\end{box_orange}
	
	\column{0.5\textwidth}
	\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{img/brain_tree.jpg}
	\caption{mayoclinic.org}
	\end{figure}
\end{columns}
\footnotetext{\textbf{Petersen \textit{et} al.} \textit{Mild cognitive impairment: Ten years later}, Archives of Neurology, 2009}
\end{frame}

\begin{frame}{MCI and Alzheimer's disease (AD)}
\begin{columns}
	\column{0.5\textwidth}
		{\Large MCI is a \textbf{prodromal} stage of \textbf{Alzheimer's disease}.}
		\vspace{2.5em}
		\begin{box_blue}{What is AD?}
			\begin{itemize}
				\item A \textbf{neurodegenerative} disease
				\item Most common type of dementia
			\end{itemize}
		\end{box_blue}
	\column{0.5\textwidth}
	\begin{figure}
	\centering
%	\includegraphics[width=0.9\textwidth]{img/brain_ad}
	\includegraphics[width=\textwidth]{img/brain_ad_2}
	\caption{\quad\quad\quad Left: Healthy \quad-\quad Right: Alzheimer's}
	\end{figure}
\end{columns}
\end{frame}

% Alternative futures
\begin{frame}
\vfill
 \begin{columns}
  \column{0.43\textwidth}
    \centering
    {\Large\textbf{MCI as a prodromal stage}}
    \begin{figure}
     \centering
     \includegraphics[width=\textwidth]{alternative_futures/1_v2.png}
    \end{figure}
  \column{0.5\textwidth}
    \begin{box_blue}{\Large\textsc{Why progression?}}
    \vspace{0.5em}
     MCI patients progress to AD at a rate in the range of {\Large 10 to 33.6\%} per year.\footnotemark \footnotemark\\
     \footnotetext[2]{\tiny\textbf{Bruscoli, M., \& Lovestone.} \textit{Is MCI really just early dementia? \\A systematic review of conversion studies.}, International \\Psychogeriatrics, 2004}
    \footnotetext[3]{\tiny\textbf{Ward \textit{et} al.} \textit{Rate of Conversion from Prodromal Alzheimer's \\Disease to Alzheimer's Dementia: A Systematic Review of the \\Literature}, Dementia and Geriatric Cognitive Disorders Extra, 2013}
    \end{box_blue}
 \end{columns}
\end{frame}

\begin{frame}{Diagnosing Alzheimer's}
\begin{columns}
	\column{0.6\textwidth}
    \begin{itemize}[<+->]
    \item<1-> Main battery relies on \textbf{neuropsychological tests}\footnotemark \only<1>{\\ {\scriptsize \hspace*{0.5cm} \textsc{CDR - MMSE - GDS-FAST $\cdots$}}}
    \item<2-> Medical structural imaging techniques \\ {\scriptsize \hspace*{0.5cm} \textsc{MRI - CT - PET} $\cdots$}
    \item<2-> {\color{orange_bg} Some researches have also focused on biomarkers} \\ {\scriptsize \hspace*{0.5cm} \textsc{Lab tests - Genetics} $\cdots$}
    \end{itemize}
    \column{0.4\textwidth}
      \only<1>{
      \begin{figure}
      \centering
      \includegraphics[width = \textwidth]{img/psy1.jpg}
      \end{figure}
      }
      \only<2->{
      \begin{figure}
      \centering
      \includegraphics[width = \textwidth]{img/mri.jpg}
      \end{figure}
      }
\end{columns}
\only<3->{
\vskip-4.2cm
  \begin{box_orange}{\textsc{Structural MRI}}
  "...provide additional information about abnormal tissue atrophy or other abnormal biomarkers that can be sensitively detected at the early stage of the disease..."\footnotemark \\
  
  \footnotetext[3]{\textbf{Long \textit{et.} al.} \textit{Prediction and classification of Alzheimer disease based on quantification of MRI\\ deformation} - PLOS ONE, 2017}
  \end{box_orange}
}
\only<1-2>{
\footnotetext{\textbf{G.M. McKhann \textit{et} al.} \textit{The diagnosis of dementia due to Alzheimer’s disease: Recommendations from the National Institute on Aging-Alzheimer’s Association workgroups on diagnostic guidelines for Alzheimer's disease}, Alzheimer’s \& Dementia, 2011}
}
\end{frame}


% Why MRI and Hypothesis
\begin{frame}{Hypothesis}
\vfill
 \begin{columns}
  \column{0.52\textwidth}
    \vspace{1em}
    \begin{box_blue}{\textsc{Sulci relevance in progression}}
        There are relationships between sulcal depth, shape and the neurodegenerative process of AD.\footnotemark
    \end{box_blue}
  \column{0.45\textwidth}
        \textbf{Sulci depth and shape} can describe the \textbf{progression from MCI to AD}.
        \begin{figure}
		\centering
		\includegraphics[width=0.9\textwidth]{img/sulcus_gyrus}
% 		\caption{Pearson education}
	\end{figure}
%     \end{box_blue}
 \end{columns}
 \footnotetext[5]{\tiny\textbf{Im \textit{et.} al.} \textit{Sulcal morphology changes and their relationship with cortical thickness and gyral white matter volume in mild cognitive impairment and Alzheimer's disease.} NeuroImage, 2008}
\end{frame}




% === Our proposal ===
\section{Our proposal}
\framecard[light_blue]{{\color{white} \Huge{\textbf{OUR PROPOSAL}}}}
\begin{frame}{Our Proposal}
\begin{columns}
	\column{0.4\textwidth}
    \begin{figure}
    \centering
    \includegraphics[width = 0.8\textwidth]{img/idea.png}
    \end{figure}
    
    \column{0.6\textwidth}
    {\Large A method to characterize sulcal patterns involved in the progression from \textbf{mild cognitive impairment} to \textbf{Alzheimer's disease}.}
\end{columns}
\end{frame}

%==============================
% APPROACH 1
%==============================
{
\setbeamertemplate{background}{}
\usebackgroundtemplate{\includegraphics[width=\paperwidth]{img/approach_1_bg}}
\begin{frame}[plain]
\vfill
\vspace{3em}
\textcolor{white}{\huge{\textbf{FIRST APPROACH:\\[1em]}}\LARGE{Characterizing brain patterns \\[0.05em]in conversion from mild cognitive \\[0.2em]impairment to Alzheimer's disease}}
\vfill
\end{frame}
}

\begin{frame}{Pipeline}
\begin{columns}
	\column{0.31\textwidth}
    \begin{box_blue}{\Large ROI Extraction}
    \begin{figure}
    \centering
    \includegraphics[width = \textwidth]{img/freesurfer}
    \end{figure}
    \end{box_blue}
    
    \column{0.38\textwidth}
    \begin{box_green}{\Large Feature Extraction}
    \begin{figure}
    \centering
    \includegraphics[width = 0.9\textwidth]{img/differences_sulci.png}
    \end{figure}
    \end{box_green}
    
    \column{0.31\textwidth}
    \begin{box_orange}{\Large Validation}
    \begin{figure}
    \centering
    \includegraphics[width = \textwidth]{img/svm_pic.jpg}
    \end{figure}
    \end{box_orange}
\end{columns}
\end{frame}

% Methods
\framecard[white]{{\color{blue_bg} \Huge{\textbf{1. Automatic ROI extraction}}}}
\begin{frame}{ROI Extraction: Class modeling}
\begin{columns}
	\column{0.4\textwidth}
		\begin{figure}
		\centering
		\only<1>{\includegraphics[height = 0.75\textheight]{img/reshape}}
		\only<2>{\includegraphics[height = 0.75\textheight]{img/matrix}}
		\end{figure}

	\column{0.6\textwidth}
	\begin{enumerate}
	\item<1-|alert@1> Reshaping each volume into a 1D-column vector
	\item<2-|alert@2> The set of reshaped volumes constitutes a matrix $X$
	\end{enumerate}
\end{columns}
\end{frame}

\begin{frame}{ROI Extraction: Class modeling}
% \framesubtitle{Model construction}
We can extract a model per each group (MCInc and MCIc) by modeling the subjects as a \textbf{linear combination}:
{\Large
\begin{equation}
X = WH
\end{equation}}
Where matrix $W$ contains the \alert{model} per class and $H$ is a matrix of weights given by the \textit{labels}.
\end{frame}

\begin{frame}{ROI Extraction: Class modeling}
\only<1>{
	\begin{figure}
	\centering
	\includegraphics[width = 1.05\textwidth]{img/matrix_model}
	\end{figure}
}
\only<2>{
	\begin{figure}
	\centering
	\includegraphics[width = 0.75\textwidth]{img/matrix_model}
	\end{figure}

    \begin{box_blue}{}
    Where $\mathbf{W}$ can be obtained as $\mathbf{W} = \mathbf{XH}^{T} (\mathbf{HH}^{T})^{-1}$.
    \end{box_blue}
}
\end{frame}

\begin{frame}{ROI Extraction}
\only<1>{
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{img/roi_extracted.png}
        \caption{Map of differences obtained in the ROI extraction.}
    \end{figure}
}
\only<2->{
	\centering
    \begin{tikzpicture}
    \node (img1) {\includegraphics[width = 0.9\textwidth]{img/roi1}};
    \node (img2) {\includegraphics[width = 0.9\textwidth]{img/roi2}};
    \node (img3) {\includegraphics[width = 0.9\textwidth]{img/roi3}};
    \end{tikzpicture}
}
\end{frame}

\framecard[white]{{\color{blue_bg} \Huge{\textbf{2. Feature extraction}}}}
\begin{frame}{ROI Pattern Extraction}
\begin{columns}
	\column{0.5\textwidth}
        \begin{enumerate}
            \item<2-|alert@2> Gradient calculation
            \item<3-|alert@3> Cubical meshing of $16\times16\times16$ vox$^{3}$.
            \item<4-|alert@4> Maximum gradient extraction per block.
            \item<5-|alert@5> Feature vector assembly.
            \item<6-|alert@6> Repeat for a meshing of $8\times8\times8$.
        \end{enumerate}
	\column{0.5\textwidth}
        \only<1>{
        \begin{figure}
        \centering
        \includegraphics[width = 0.8\textwidth]{img/meshing0}
        \end{figure}
        }
        \only<2>{
        \begin{figure}
        \centering
        \includegraphics[width = 0.8\textwidth]{img/gradient0}
        \end{figure}
        }
        \only<3>{
        \begin{figure}
        \centering
        \includegraphics[width = 0.8\textwidth]{img/meshing}
        \end{figure}
        }
        \only<4>{
        \begin{figure}
        \centering
        \includegraphics[height = 0.75\textheight]{img/gradient}
        \end{figure}
        }
        \only<5>{
        \begin{box_blue}{}
            Let ${r_{ij}} = \max \left( {{\mathbf{R}_{ij}}} \right)$. Where $\mathbf{R}_{ij}$ contains the magnitudes at the block $(i,j)$. The feature vector $\mathbf{f}$ was defined as:
            \begin{equation*}
                \mathbf{f} = \bigcup_{i, j} \left\{ {r_{ij},{\theta _{ij}},{\phi _{ij}}} \right\}
            \end{equation*}
        \end{box_blue}
        }
        \only<6>{
        \begin{figure}
        \centering
        \includegraphics[width = 0.8\textwidth]{img/meshing}
        \end{figure}
        }
\end{columns}
\end{frame}


\framecard[white]{{\color{orange_bg} \Huge{\textbf{3. Validation}}}}
\begin{frame}{Validation}
To validate the characterization, we used a support vector machine classifier (SVM). The classifier was trained with the following parameters:
\begin{columns}
	\column{0.4\textwidth}
	\begin{figure}
	\centering
	\includegraphics[width = 0.75\textwidth]{img/classifier}
	\end{figure}
	\column{0.6\textwidth}
	\begin{itemize}
		\item Polynomial Kernel
		\item Order: 4th
		\item $\Gamma = 0.01$
		\item Leave-one-out cross validation
	\end{itemize}
\end{columns}
\end{frame}

\framecard[green_bg]{{\color{white} \Huge{\textbf{RESULTS}}}}
\begin{frame}{Dataset}
\vskip2em
Data used in the preparation of this work were obtained from the Alzheimer’s Disease Neuroimaging Initiative (ADNI) database.
    \begin{table}[]
	\centering
    \begin{tabular}{l|l|l|l|l|}
    \cline{2-5}
                                            & \multicolumn{2}{c|}{\textbf{MCInc}}                                        & \multicolumn{2}{c|}{\textbf{MCIc}}                                        \\ \cline{2-5} 
                                            & \multicolumn{1}{c|}{\textbf{Male}} & \multicolumn{1}{c|}{\textbf{Female}} & \multicolumn{1}{c|}{\textbf{Male}} & \multicolumn{1}{c|}{\textbf{Female}} \\ \hline
    \multicolumn{1}{|l|}{\textbf{Subjects}} & 104                                & 76                                   & 100                                & 56                                   \\ \hline
    \multicolumn{1}{|l|}{\textbf{Age}}      & $75.6 \pm 6.7$                     & $73.4 \pm 7.2$                       & $75.8 \pm 6.2$                     & $74.6 \pm 7.4$                       \\ \hline
    \end{tabular}
    \end{table}
    
    %% ADNI Logo
    \begin{figure}
    \centering
    \includegraphics[width = 0.2\textwidth]{img/adni_logo}
    \end{figure}
\end{frame}

\begin{frame}{Preprocessing}
\vskip1.5em
This step is applied in order to harmonize the data and take it to a reference space.\footnotemark
\vskip-0.5em
	\begin{columns}
		\column{0.25\textwidth}
		  \begin{figure}
		    \centering
		    \includegraphics[width = \textwidth]{preprocessing/1}
		    \caption{A. RAW image}
		  \end{figure}
		\column{0.25\textwidth}
		  \begin{figure}
		    \centering
		    \includegraphics[width = \textwidth]{preprocessing/2}
		    \caption{B. Intensity normalization}
		  \end{figure}
		\column{0.25\textwidth}
		  \begin{figure}
		    \centering
		    \includegraphics[width = \textwidth]{preprocessing/3}
		    \caption{C. Skull stripping}
		  \end{figure}
		\column{0.25\textwidth}
		  \begin{figure}
		    \centering
		    \vskip0.8em
		    \includegraphics[width = \textwidth]{preprocessing/6}
		    \caption{D. Rigid registration to MNI152}
		  \end{figure}
	\end{columns}
\footnotetext{\textbf{Dale, Fischl and Serno}. \textit{``Cortical Surface-Based Analysis: I. Segmentation and Surface Reconstruction.''} NeuroImage, 1999}
\end{frame}

\begin{frame}
\frametitle{ROI Extraction: map of differences}
\begin{columns}
	\column{0.45\textwidth}
	\begin{itemize}
	\item<1-|alert@1> Ventricular system
	\item<2-|alert@2> Hippocampal regions
	\item<3-|alert@3> Occ-temp cortex\footnotemark
	\end{itemize}
	\column{0.55\textwidth}
	\begin{figure}
	\centering
		\only<1>{\includegraphics[width = 0.75\textwidth]{img/res_roi1}}
		\only<2>{\includegraphics[width = 0.75\textwidth]{img/res_roi2}}
		\only<3>{\includegraphics[width = 0.75\textwidth]{img/res_roi3}}
	\end{figure}
\end{columns}
\footnotetext{Highly variable anatomical structures.}
\end{frame}

\begin{frame}{Classification results}
\vspace{2em}
\begin{columns}
 \column{0.5\textwidth}
    \LARGE
    \begin{table}[]
        \begin{tabular}{ll}
        \hline
        \multicolumn{1}{c}{\textbf{Gender}} & \multicolumn{1}{c}{\textbf{AUC}} \\ \hline
        Male                                & 0.62                             \\
        \textcolor{orange_bg}{Female}                              & \textcolor{orange_bg}{0.86}                             \\ \hline
        \end{tabular}
    \end{table}
 \column{0.5\textwidth}
    \vfill
    \begin{table}[]
    \begin{tabular}{lcc}
    \hline
                & \textbf{Male} & \textbf{\textcolor{orange_bg}{Female}} \\ \hline
    \textbf{MCInc} & 104           & \textcolor{orange_bg}{76}              \\
    \textbf{MCIc}  & 100           & \textcolor{orange_bg}{56}              \\ \hline
    \end{tabular}
    \caption{Number of subjects}
    \end{table}
\end{columns}
\onslide<2>{
    \begin{box_blue}{}
     \Large Suggesting that there exist anatomical patterns associated to the progression.
    \end{box_blue}
}
\end{frame}

\begin{frame}{Product: SIPAIM 2017}
    \begin{figure}
        \centering
        \includegraphics[width=0.8\textwidth]{papers/paper_1.png}
    \end{figure}
\end{frame}

%==============================
% APPROACH 2
%==============================
{
\setbeamertemplate{background}{}
\usebackgroundtemplate{\includegraphics[width=\paperwidth]{img/approach_2_bg}}
\begin{frame}[plain]
\vfill
\vspace{3em}
\begin{flushright}
    \textcolor{white}{\huge{\textbf{SECOND APPROACH:\\[1em]}}\LARGE{Sulci characterization to predict \\progression from mild cognitive \\[0.3em]impairment to Alzheimer's disease}}
\end{flushright}
\vfill
\end{frame}
}

% === Our proposal ===
\section{Methods}
% \framecard[green_bg]{{\color{white} \Huge{\textbf{METHODOLOGY}}}}
\begin{frame}{Pipeline}
\begin{columns}
	\column{0.31\textwidth}
    \begin{box_blue}{\Large Preprocessing}
    \begin{figure}
    \centering
    \includegraphics[width = \textwidth]{img/freesurfer}
    \end{figure}
    \end{box_blue}
    
    \column{0.38\textwidth}
    \begin{box_green}{\Large Sulci Characterization}
    \begin{figure}
    \centering
    \includegraphics[width = 0.9\textwidth]{img/msa.png}
    \end{figure}
    \end{box_green}
    
    \column{0.31\textwidth}
    \begin{box_orange}{Validation}
    \begin{figure}
    \centering
    \includegraphics[width = \textwidth]{img/svm_pic.jpg}
    \end{figure}
    \end{box_orange}
\end{columns}
\end{frame}

%\begin{frame}{1. Preprocessing}
%\begin{columns}
%\column{0.5\textwidth}	
%		\only<1>{
%		\begin{figure}
%		\centering
%		\includegraphics[width = 0.8\textwidth]{preprocessing/1}
%		\end{figure}
%		}
%		\only<2>{
%		\begin{figure}
%		\centering
%		\includegraphics[width = 0.8\textwidth]{preprocessing/2}
%		\end{figure}
%		}
%        \only<3>{
%		\begin{figure}
%		\centering
%		\includegraphics[width = 0.8\textwidth]{preprocessing/3}
%		\end{figure}
%		}
%        \only<4>{
%		\begin{figure}
%		\centering
%		\includegraphics[width = 0.8\textwidth]{preprocessing/6}
%		\end{figure}
%		}
%     
%	\column{0.5\textwidth}
%    \textsc{Over each MRI is performed...}\footnotemark
%	\begin{itemize}
%	\item<2-|alert@2> Intensity Normalization
%	\item<3-|alert@3> Skull stripping 
%	\item<4-|alert@4> Rigid registration of each case to a MNI152 template
%	\end{itemize}
%
%\end{columns}
%\footnotetext{\textbf{Dale, Fischl and Serno}. \textit{Cortical Surface-Based Analysis: I. Segmentation and Surface Reconstruction.} NeuroImage, 1999}
%\end{frame}


\begin{frame}{1. Preprocessing}
    \begin{center}
    \textcolor{orange_bg}{\Large SULCI EXTRACTION}\\
    Sulci should be better observed in an edge space \textbf{(Sobel's magnitude space)}.
    \end{center}
    \begin{figure}
        \centering
        \vskip-0.4cm
        \includegraphics[width = 0.95\textwidth]{img/gradients_zoomed.png}
    \end{figure}
\end{frame}


%% Center location
%\begin{frame}{1. Preprocessing}
%\begin{box_green}{\textsc{Center location}}
%\begin{columns}
%	\column{0.45\textwidth}
%    	\begin{itemize}
%    	\item A center $C(x_0, y_0, z_0)$ is defined as a global reference. \\{\footnotesize \textsc{Centroid of MNI152 brain mask}}
%    	\end{itemize}
%    \column{0.55\textwidth}
%    	\begin{figure}
%    	\centering
%        \includegraphics[width = \textwidth]{img/center_def.png}
%    	\end{figure}
%\end{columns}
%\end{box_green}
%\end{frame}


\framecard[white]{{\color{green_bg} \Huge{\textbf{2. Sulci characterization}}}}

% \begin{frame}{2. Sulci characterization}
%  \begin{columns}
%   \column{0.57\textwidth}
%      \textcolor{orange_bg}{{\Large\textbf{Spherical Analysis}}\\[0.8em]}
%     A set of \textbf{hollow spheres} is defined with fixed center (MNI152 centroid), thickness and a different radii.
%   \column{0.43\textwidth}
%     \only<1>{
%     \begin{figure}
%     \centering
%     \hskip-2.6cm
%     \includegraphics[height = 0.6\textheight]{spheres/1}
%     \end{figure}
%     }
%     \only<2>{
%     \begin{figure}
%     \centering
%     \hskip-2.6cm
%     \includegraphics[height = 0.6\textheight]{spheres/3}
%     \end{figure}
%     }
%     \only<3>{
%     \begin{figure}
%     \centering
%     \hskip-2.6cm
%     \includegraphics[height = 0.6\textheight]{spheres/4}
%     \end{figure}
%     }
%     \only<4>{
%     \begin{figure}
%     \centering
%     \hskip-2.6cm
%     \includegraphics[height = 0.6\textheight]{spheres/5}
%     \end{figure}
%     }
%     \only<5>{
%     \begin{figure}
%     \centering
%     \hskip-2.6cm
%     \includegraphics[height = 0.6\textheight]{spheres/6}
%     \end{figure}
%     }
%  \end{columns}
% \end{frame}

\begin{frame}{2. Sulci characterization}
  \begin{columns}
  \column{0.57\textwidth}
     \textcolor{orange_bg}{{\Large\textbf{Intersection}}\\[1em]}
     A set of \textbf{hollow spheres} is defined with fixed center (MNI152 centroid), thickness and a different radii.\\[2em]
     Intersected information by the hollow sphere and the gradient space is extracted.
  \column{0.4\textwidth}
    \vskip-2em
    \animategraphics[loop, autoplay, width=\linewidth]{1}{spheres/animation/intersection-}{1}{4}
  \end{columns}
\end{frame}

\begin{frame}{2. Sulci characterization}
 \begin{columns}
  \column{0.57\textwidth}
    \textcolor{green_bg}{{\Large\textbf{Mapping}}\\[0.8em]}
    Each sphere is mapped onto a plane via equirectangular projection (radial average).\\[2em]
    
    \begin{box_blue}{}
        \large\textbf{Deepest sulci} are expected to remain after the projection.
    \end{box_blue}
  \column{0.4\textwidth}
    \vskip-2em
    \animategraphics[loop, autoplay, width=\linewidth]{1}{spheres/animation/mapping-}{1}{4}
 \end{columns}
\end{frame}


\framecard[white]{{\color{green_bg} \Huge{\textbf{Feature Extraction}}}}
\begin{frame}{Feature extraction: Curvelets}
\begin{columns}
	\column{0.55\textwidth}
	\only<1-2>{
	\begin{itemize}
	\item<1-2> Sulci are tipically \textit{bent-folded} structures. A \textbf{curvelet representation} should describe these structures efficiently\footnotemark.\\[1.5em]
	\item<2> Dimensionality was then reduced by representing each sub-band as the parameters of a \textbf{generalized Gaussian}.
	\end{itemize}
	}
	\only<3->{
		\begin{itemize}
			\item Sphere features were concatenated
			\begin{itemize}
				\item 3 parameters $(\mu, \sigma, \beta)$
				\item 161 sub-bands
				\item 4 spheres ($\Delta_{tk} = 25 vox$)
			\end{itemize}
			\vspace{1.5em}
			{\Large\textcolor{orange_bg}{\textbf{1932 features!}}}
		\end{itemize}
	}
	
	\column{0.45\textwidth}
	\only<1>{
	\begin{figure}
	\centering
%	\includegraphics[width = 0.4\textwidth]{spheres/5}
%	\includegraphics[width = 0.45\textwidth]{curvelet/3}
	\includegraphics[width=\textwidth]{curvelet/v2/1_lr}
	\end{figure}
	}
	\only<2->{
	\begin{figure}
	\centering
%	\includegraphics[width = \textwidth]{img/gen_gauss_1}
	\includegraphics[width=\textwidth]{curvelet/v2/2_lr}
	\end{figure}
%	\begin{equation*}
%	c(s,\theta) \overset{\underset{\mathrm{def}}{}}{=} \{ \mu, \sigma, \beta \}
%	\end{equation*}
	}

\end{columns}
\footnotetext{\textbf{Yuan \textit{et} al.} \textit{``A new method for retrieving batik shape patterns''}, Journal of the Association for Information Science and Technology, 2018}
\end{frame}



% === Validation ===
\framecard[white]{{\color{orange_bg} \Huge{\textbf{3. Method Validation}}}}
\begin{frame}{3. Method Validation}
\begin{columns}
	\column{0.5\textwidth}
        \begin{itemize}
            \item Classification algorithms are used to validate the proposed method.\\[1.5em]
            \item Feature relevance was analyzed using \textit{\textbf{minimum redundancy maximum relevance (mRMR)}}.\footnotemark
        \end{itemize}

    \column{0.5\textwidth}
%    \animategraphics[loop, autoplay, width=\linewidth]{12}{gif/dog-space/giphy-}{0}{116}
	\begin{figure}
		\centering
		\includegraphics[width=\textwidth]{img/svm_pic}
	\end{figure}
\end{columns}
\footnotetext{\textbf{Peng \textit{et} al.} \textit{``Feature selection based on mutual information: criteria of max-dependency, max-relevance, and min-redundancy''.} IEEE Transactions on Pattern Analysis and Machine Intelligence, 2005.}
\end{frame}


% === RESULTS ===
\section{Results}
\framecard[green_bg]{{\color{white} \Huge{\textbf{RESULTS}}}}

% DATASET
\begin{frame}{Dataset}
{}
{\textcolor{green_bg}{\Large 829}} participants in this study provided by ADNI initiative. Data were analyzed based on time of stability (MCInc) or conversion (MCIc)\footnotemark.\\[0.4cm]
\begin{columns}
\column{0.4\textwidth}
	\begin{figure}
		\centering
		\includegraphics[width = 0.6\textwidth]{img/adni_logo.png}
	\end{figure}
\column{0.6\textwidth}
  \begin{table}[]
  	\large
    \begin{tabular}{c|cc}
		       & \textbf{MCInc} & \textbf{MCIc} \\ \hline
    \textbf{24 months} & 339            & 205           \\
    \textbf{36 months} & 281            & 244           \\
    \textbf{60 months} & 153            & 283          
    \end{tabular}
  \end{table}
  \vspace{0.5em}
  \hspace{8em}\textbf{MCInc}: Stable \\
  \hspace{8em}\textbf{MCIc}: Conversion to AD
\end{columns}

\footnotetext{Single-visit and misdiagnosed subjects were excluded from the study.}
\end{frame}

% Qualitative results
\begin{frame}{Sulci characterization}
\vskip0.6cm
At a subcortical level, some differences may be appreciated.
\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{results/subcortical.png}
\end{figure}
\end{frame}


\begin{frame}{Validation: Sobel Space}
\begin{columns}
	\column{0.5\textwidth}
		\begin{itemize}
		\item Curvelet decomposition:
			\begin{itemize}
			\item 4 Scales
			\item 32 Angles
			\end{itemize}
		\item Random forest classifier.
		\item 10-fold cross validation.
		\end{itemize}
		\vspace{1em}
		\begin{box_blue}{}
			\small \vskip1em
			\begin{table}[]
				\begin{tabular}{l|ccc}
				\textbf{Time (Months)} & 24   & 36   & 60   \\ \hline
				\textbf{AUC}           & 0.75 & 0.73 & 0.77
				\end{tabular}
			\end{table}
		\end{box_blue}
	\column{0.5\textwidth}
		\begin{figure}
			\centering
			\vskip-1cm
			\includegraphics[width=\textwidth]{results/roc/eps/sobel_random_forest_10_folds.eps}	
		\end{figure}
\end{columns}
\end{frame}

\begin{frame}{Dimensionality reduction}
\only<2>{\vskip-1cm}
\begin{columns}
	\column{0.5\textwidth}
		\textbf{Minimum redundancy maximum relevance (mRMR)} was used to select a set of 100 from the 1932 available features.\\[1em]
		\begin{box_blue}{}
			\small \vskip1em
			\begin{table}[]
				\begin{tabular}{l|ccc}
				\textbf{Time (Months)} & 24   & 36   & 60   \\ \hline
				\textbf{AUC}           & 0.70 & 0.69 & 0.72 \\
				\textcolor{orange_bg}{\textbf{AUC Reduction}}     & \textcolor{orange_bg}{5\%} & \textcolor{orange_bg}{4\%} & \textcolor{orange_bg}{5\%}
				\end{tabular}
			\end{table}
		\end{box_blue}
	\column{0.5\textwidth}
		\begin{figure}
			\centering
			\vskip-1cm
			\includegraphics[width=\textwidth]{results/sobel_random_forest_10_folds_mrmr.png}	
		\end{figure}
\end{columns}
\only<2>{
\vskip-5.4cm
\begin{box_blue}{}
    \LARGE\centering
    \vspace{0.7cm}
    \textcolor{orange_bg}{\textbf{Dropping 95\% of the features reduced only a 5\% in classification performance (AUC).}}
    \vspace{0.7cm}
\end{box_blue}

}
\end{frame}

\begin{frame}{Other methods}
  \begin{table}[]
    \begin{tabular}{|l|l|l|l|l|}
    \hline
    \textbf{Method}                   & \textbf{MCIc/MCInc} & \textbf{Data source}                                                                  & \textbf{Author (year)}       & \textbf{AUC}  \\ \hline
    \begin{tabular}[c]{@{}l@{}}\textbf{Multi-Spherical}\\\textbf{analysis}\end{tabular} & \textbf{313/435}    & \textbf{MRI}                                                                          & \textbf{Silva \textit{et} al. (2019)} & \textbf{0.77} \\
    Deep Learning                     & 134/561             & \begin{tabular}[c]{@{}l@{}}Cognitive score,\\ MRI, CSF,\\demographics\end{tabular} & Lee \textit{et} al. (2019)            & 0.83          \\
    LRC	              & 70/61               & MRI                                                                                   & Huang \textit{et} al. (2017)          & 0.71 \\
    SVM+LDS	              & 164/100               & MRI                                                                                   & Moradi \textit{et} al. (2015)          & 0.76 \\
    SVM                               & 43/48               & MRI, PET, CSF                                                                         & Zhang \textit{et} al. (2012)          & 0.729         \\ \hline
    \end{tabular}
  \end{table}

{\Large\textcolor{orange_bg}{\textbf{Deep learning achieves 0.83 but, using multiple sources!}}}
\footnotetext{\textbf{LRC}: Linear regression classifier. \textbf{SVM}: Support vector machine. \textbf{LDS}: Low density separation.}
\end{frame}


\begin{frame}{Most featured spheres}
\vspace{1.5em}
\only<1>{
	Most appearing spheres after feature selection show the amount of relevant information provided at each level of the brain. 
	\begin{figure}
		\centering
		\includegraphics[width=\textwidth]{results/top_spheres.png}
	\end{figure}
	}
\only<2>{
	\begin{figure}
		\centering
		\includegraphics[width=0.85\textwidth]{spheres/scales/25_to_50_visualization.png}
	\end{figure}
	\centering
	Subcortical spheres are the most relevant according to the feature selection (figure: 25 to 50 voxels sphere).
}
\end{frame}

\begin{frame}{Product: SIPAIM 2019}
    \begin{figure}
        \centering
        \includegraphics[width=0.8\textwidth]{papers/paper_2.png}
    \end{figure}
\end{frame}

\begin{frame}{Conclusions}
This work presents a fully automatic method to characterize sulci patterns associated to the progression from MCI to AD.\\[1em]

\begin{itemize}
    \item Sulci depth and shape may be potential biomarkers associated to the progression from MCI to AD.\\[1em]
    \item Results from this research suggest differences between MCI stable and converters to AD mainly at a subcortical level.
    %\item Combining additional information seems to better discriminate subjects at risk of progression to AD.
\end{itemize}
\end{frame}

\begin{frame}{Additional products}
    \begin{itemize}
        \item \textit{``Federated Learning in Distributed Medical Databases: Meta-Analysis of Large-Scale Subcortical Brain Data''}. 2019 IEEE 16th International Symposium on Biomedical Imaging (ISBI 2019). DOI:\href{https://www.doi.org/10.1109/ISBI.2019.8759317}{10.1109/ISBI.2019.8759317}\\[1em]

    \item \textit{``Discriminating Cerebral Palsy by quantifying Ocular motion''}. 15th International Conference on Medical Information Processing and Analysis - SIPAIM. 2019. (Accepted).
    \end{itemize}
\end{frame}

% ==============================
% ACKNOWLEDGEMENTS
% ==============================
\framecard[blue_bg]{{\color{white} \Huge{\textbf{ACKNOWLEDGEMENTS}}}}
{
\setbeamertemplate{background}{}
\usebackgroundtemplate{\includegraphics[width=\paperwidth]{ack/familia}}
\begin{frame}[fragile]
\end{frame}
}
{
\setbeamertemplate{background}{}
\usebackgroundtemplate{\includegraphics[width=\paperwidth]{ack/directores}}
\begin{frame}[fragile]
\end{frame}
}

% Closing
{
\setbeamertemplate{background}{}
\usebackgroundtemplate{\includegraphics[width=\paperwidth]{ack/thanks}}
\begin{frame}[plain]
\vfill
\vspace{3em}
\textcolor{white}{\huge{\textbf{THANK YOU!}}}
\vfill
\end{frame}
}

% Comments
% {
% \setbeamertemplate{background}{}
% \usebackgroundtemplate{\includegraphics[width=\paperwidth]{img/carl_sagan}}
% \begin{frame}[fragile]
% \end{frame}
% }

\framecard[green_bg]{{\color{white} \Huge{\textbf{APPENDIX}}}}
\begin{frame}{Future work}
Associated to this research future work may involve:
\begin{columns}
	\column{0.6\textwidth}
	\begin{itemize}
	  \item Anatomical/morphological interpretation\\
	  (sub-band selection)
	  \item Progression temporal modeling\\
	  (longitudinal)
	  \item Adding more sources\\
	  (PET, Cognitive, CSF, genetics)
	\end{itemize}
	\column{0.4\textwidth}
	\begin{figure}
		\includegraphics[width=\textwidth]{img/thoughtful-monkey}
	\end{figure}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Classification results}
\begin{columns}
	\column{0.6\textwidth}
	\begin{figure}
	\centering
	\includegraphics[width = \textwidth]{img/roc.pdf}
	\end{figure}
	\column{0.4\textwidth}
	\onslide<1->{
	\begin{table}[!ht]
		\centering
		\begin{tabular}{l l}
		\\
		\hline
		\multicolumn{1}{c}{\textbf{Group}} & \multicolumn{1}{c}{\textbf{AUC\footnotemark}} \\ \hline
		 \textcolor{femaleColor}{Female} & \textcolor{femaleColor}{0.86}\\
         \textcolor{maleColor}{Male} 	& \textcolor{maleColor}{0.62}\\ \hline
		\end{tabular}
		\end{table}
		}
\end{columns}
\footnotetext{Number of subjects. \textbf{Female (MCInc | MCIc):} 76 | 56. \textbf{Male (MCInc | MCIc):} 104 | 100.}
\end{frame}

\begin{frame}{First approach in new dataset}
\vspace{1em}
\only<1>{
\begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{results/first_approach/map_of_differences}
    \caption{Map of differences}
\end{figure}
}
\only<2>{
\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{results/first_approach/features_36_months.eps}
    \caption{36 Months}
\end{figure}
}
\only<3>{
\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{results/first_approach/features_48_months.eps}
    \caption{48 Months}
\end{figure}
}
\only<4>{
\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{results/first_approach/features_60_months.eps}
    \caption{60 Months}
\end{figure}
}
\end{frame}

\begin{frame}{Curvelet Transform}
 It is an approach for harmonic analysis whose concept relies on performing parabolic dilations, rotations and translations of a especifically shaped function $\Psi$ (basis).\footnotemark
 \vskip-1.5em
 \begin{columns}
  \column{0.5\textwidth}
    \begin{eqnarray*}
        \Psi_{a,b,\theta}(\chi) &=& a^{-3/4}\Psi(D_a R_\theta (\chi - b)) \\
        D_a &=&   \begin{pmatrix}
                    {1/a} & {0} \\ 
                    {0}   & {1/\sqrt(a)} 
                \end{pmatrix}
    \end{eqnarray*}
  \column{0.5\textwidth}
    \begin{figure}
     \centering
     \includegraphics[width=0.7\textwidth]{img/curvetet}
    \end{figure}

 \end{columns}
\footnotetext{\textbf{E. J. CANDÈS and L. DEMANET}, Curvelets and Fourier integral operators, \textit{C. R. Math. Acad}. Sci. Paris 336 (2003), 395–398.}
\end{frame}



% =====================================
% END OF DOCUMENT
% =====================================
\end{document}
